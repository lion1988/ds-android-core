package com.ds.base_core.java.mvp.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ds.base_core.java.mvp.interfaces.BaseFragmentContract;
import com.ds.base_core.java.mvp.presenter.BaseFragmentPresenter;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public abstract class BaseDaggerFragment<P extends BaseFragmentPresenter, CP extends BaseFragmentContract.IBaseFragmentPresenter> extends DaggerFragment implements BaseFragmentContract.IBaseFragmentView<CP> {
    @Inject
    protected P presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
        initData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        deInit();
    }

    @Override
    public void deInit() {

    }

    @Override
    public void showProgress(String message) {
        //  TODO:
    }

    @Override
    public void hideProgress() {
        //  TODO:
    }
}
