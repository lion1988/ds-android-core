package com.ds.base_core.java.mvp.presenter;

import com.ds.base_core.java.mvp.interfaces.BaseFragmentContract;

public class BaseFragmentPresenter<V extends BaseFragmentContract.IBaseFragmentView> extends BasePresenter<V> implements BaseFragmentContract.IBaseFragmentPresenter<V> {
}
