package com.ds.base_core.java.mvp.presenter;

import com.ds.base_core.java.mvp.interfaces.IBasePresenter;
import com.ds.base_core.java.mvp.interfaces.IBaseView;

public class BasePresenter<V extends IBaseView> implements IBasePresenter<V> {
    protected V v;

    @Override
    public void initData(V view) {
        v = view;
    }

    @Override
    public void deInit() {
    }
}
