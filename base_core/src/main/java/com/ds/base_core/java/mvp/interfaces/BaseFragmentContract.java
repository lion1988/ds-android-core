package com.ds.base_core.java.mvp.interfaces;

import android.view.View;

public interface BaseFragmentContract {
    interface IBaseFragmentPresenter<V extends IBaseFragmentView> extends IBasePresenter<V> {
    }

    interface IBaseFragmentView<CP extends IBaseFragmentPresenter> extends IBaseView<CP> {
        void initUI(View view);

        void showProgress(String message);

        void hideProgress();
    }
}
