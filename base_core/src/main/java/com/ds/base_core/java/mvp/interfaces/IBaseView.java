package com.ds.base_core.java.mvp.interfaces;

public interface IBaseView<P extends IBasePresenter> {
    int getLayoutId();

    void initData();

    void deInit();
}
