package com.ds.base_core.java.mvp.interfaces;

public interface BaseActivityContract {
    interface IBaseActivityPresenter<V extends IBaseActivityView> extends IBasePresenter<V> {
    }

    interface IBaseActivityView<CP extends IBaseActivityPresenter> extends IBaseView<CP> {
        void iniUI();
    }
}
