package com.ds.base_core.java.mvp.presenter;

import com.ds.base_core.java.mvp.interfaces.BaseActivityContract;

public class BaseActivityPresenter<V extends BaseActivityContract.IBaseActivityView> extends BasePresenter<V> implements BaseActivityContract.IBaseActivityPresenter<V> {
}
