package com.ds.base_core.java.mvp.interfaces;

public interface IBasePresenter<V extends IBaseView> {
    void initData(V view);

    void deInit();
}
