package com.ds.base_core.java.mvp.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.ds.base_core.java.mvp.interfaces.BaseActivityContract;
import com.ds.base_core.java.mvp.presenter.BaseActivityPresenter;

import dagger.android.DaggerActivity;

public abstract class BaseDaggerActivity<P extends BaseActivityPresenter, CP extends BaseActivityContract.IBaseActivityPresenter> extends DaggerActivity implements BaseActivityContract.IBaseActivityView<CP> {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        iniUI();
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deInit();
    }

    @Override
    public void deInit() {

    }
}
