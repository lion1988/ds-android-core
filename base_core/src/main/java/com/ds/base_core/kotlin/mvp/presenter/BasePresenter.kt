package com.ds.base_core.kotlin.mvp.presenter

import com.ds.base_core.kotlin.mvp.interfaces.IBasePresenter
import com.ds.base_core.kotlin.mvp.interfaces.IBaseView

class BasePresenter<V : IBaseView<*>?> : IBasePresenter<V> {
    protected var v: V? = null

    override fun initData(view: V) {
        v = view
    }

    override fun deInit() {}
}