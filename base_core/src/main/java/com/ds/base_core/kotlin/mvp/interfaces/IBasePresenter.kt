package com.ds.base_core.kotlin.mvp.interfaces

interface IBasePresenter<V : IBaseView<IBasePresenter<V>>> {
    fun initData(view: V)
    fun deInit()
}