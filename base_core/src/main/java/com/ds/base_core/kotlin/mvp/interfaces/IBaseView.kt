package com.ds.base_core.kotlin.mvp.interfaces

interface IBaseView<P : IBasePresenter<*>> {
    val layoutId: Int
    fun initData()
    fun resume()
    fun pause()
    fun deInit()
}