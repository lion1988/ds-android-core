package com.ds.base;

import com.ds.base_core.java.mvp.interfaces.BaseFragmentContract;

public interface TestContract {
    interface ITestPresenter extends BaseFragmentContract.IBaseFragmentPresenter<ITestView> {
        void loadTestData();
    }

    interface ITestView extends BaseFragmentContract.IBaseFragmentView<ITestPresenter> {
        void showTitle(String s);
    }
}
