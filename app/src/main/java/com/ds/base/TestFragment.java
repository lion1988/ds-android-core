package com.ds.base;

import android.view.View;
import android.widget.TextView;

import com.ds.base_core.java.mvp.fragment.BaseDaggerFragment;

public class TestFragment extends BaseDaggerFragment<TestPresenter, TestContract.ITestPresenter> implements TestContract.ITestView {
    private TextView tvTitle;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_test;
    }

    @Override
    public void initUI(View view) {
        tvTitle = view.findViewById(R.id.tvTitle);
    }

    @Override
    public void initData() {
        presenter.initData(this);
    }

    @Override
    public void showTitle(String s) {
        tvTitle.setText(s);
    }
}
