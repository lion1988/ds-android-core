package com.ds.base;

import com.ds.base_core.java.mvp.presenter.BaseFragmentPresenter;

public class TestPresenter extends BaseFragmentPresenter<TestContract.ITestView> implements TestContract.ITestPresenter {
    @Override
    public void loadTestData() {
        v.showProgress("Loading..");
        v.showTitle("Test");
        v.hideProgress();
    }
}
